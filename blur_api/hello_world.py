# File for the 'simple' use case

import json
import botocore.vendored.requests as requests

# my own python file
from auth import GITLAB_TOKEN

def lambda_handler(event, context):
    headers = {
        "PRIVATE-TOKEN": GITLAB_TOKEN
    }

    project_id = event['queryStringParameters']['project_id']
    
    response = requests.get(
        url=f"https://gitlab.com/api/v4/projects/{project_id}/members",
        headers=headers)
    
    return {
        'statusCode': 200,
        'body': response.json()
    }
