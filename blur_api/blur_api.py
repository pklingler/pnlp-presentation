import json
import os

import botocore.vendored.requests as requests
import boto3

from image import save_and_blur_image_from_url

# my own python file
from auth import GITLAB_TOKEN

s3 = boto3.resource('s3')

def upload_to_s3(member, project_id):
    avatar_url = member['avatar_url']
    image_filename = f"{member['username']}.png"

    save_and_blur_image_from_url(avatar_url, "/tmp/"+ image_filename)

    result = s3.meta.client.upload_file(
        "/tmp/"+image_filename,
        os.getenv('S3_BUCKET'),
        f'project_{project_id}/{image_filename}')

def lambda_handler(event, context):
    headers = {
        "PRIVATE-TOKEN": GITLAB_TOKEN
    }

    project_id = event['pathParameters']['project_id']
    
    members = requests.get(
        url=f"https://gitlab.com/api/v4/projects/{project_id}/members",
        headers=headers).json()
    
    for member in members:
        upload_to_s3(member, project_id)
    
    return {
        'statusCode': 200,
        'body': "{}"
    }
