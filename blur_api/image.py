import botocore.vendored.requests as requests

from io import BytesIO

from PIL import Image
from PIL import ImageFilter

def save_and_blur_image_from_url(image_url, filename):
    response = requests.get(image_url)

    img = Image.open(BytesIO(response.content)).filter(ImageFilter.GaussianBlur(6))
    img.save(filename)

if __name__ == "__main__":
    save_and_blur_image_from_url(
        "https://assets.gitlab-static.net/uploads/-/system/user/avatar/2748996/avatar.png",
        "pklingler.png")
